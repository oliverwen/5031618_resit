package com.example.oliver.weatherapp.data;

import org.json.JSONObject;

/**
 * Created by Oliver on 15/06/2015.
 */

//Condition class that implements the JSON Populator interface
public class Condition implements JSONPopulator {
    private int code; //Declare an integer that is going to store the code from Yahoo
    private int temperature; //Declare an integer that is going to store the temperature from Yahoo
    private String description; //Declare an integer that is going to store the description from Yahoo

    //Getter that is going to get the code
    public int getCode() {
        return code;
    }

    //Getter that is going to get the temperature
    public int getTemperature() {
        return temperature;
    }

    //Getter that is going to get the description
    public String getDescription() {
        return description;
    }

    //Method that is going to populate the variables using the JSON Object data
    @Override
    public void populate(JSONObject data) {
        code = data.optInt("code"); //Store the code in the variable code
        temperature = data.optInt("temp"); //Store the temperature in the variable temperature
        description = data.optString("text"); //Store the data in the variable integer
    }
}

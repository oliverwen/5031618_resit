package com.example.oliver.weatherapp;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.oliver.weatherapp.data.Channel;
import com.example.oliver.weatherapp.data.Item;
import com.example.oliver.weatherapp.service.WeatherServiceCallback;
import com.example.oliver.weatherapp.service.YahooWeatherService;

//This class implements the interface WeatherServiceCallback
public class MainActivity extends ActionBarActivity implements WeatherServiceCallback {

    private ImageView weatherIconImageView; //Declare an ImageView that stores the weather icon
    private TextView temperatureTextView; //Declare a TextView that stores the temperature
    private TextView conditionTextView; //Declare a TextView that stores the condition
    private TextView locationTextView; //Declare a TextView that stores the location

    private YahooWeatherService service; //Store the YahooWeatherService in service
    private ProgressDialog dialog; //Declare a progress dialog

    //Method that creates the bundle savedInstanceState
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); //create the savedInstanceState
        setContentView(R.layout.activity_main); //Set the content view to the layout of activity_main

        //Find the corresponding views by id to each of the variables
        weatherIconImageView = (ImageView) findViewById(R.id.weatherIconImageView);
        temperatureTextView = (TextView) findViewById(R.id.temperatureTextView);
        conditionTextView = (TextView) findViewById(R.id.conditionTextView);
        locationTextView = (TextView) findViewById(R.id.locationTextView);

        //Store a new Yahoo Weather Service in service
        service = new YahooWeatherService(this);

        dialog = new ProgressDialog(this); //Store a new progress dialog in dialog
        dialog.setMessage("Loading..."); //Set the message as "Loading..."
        dialog.show(); //Show the dialog

        service.refreshWeather("Coventry, UK"); //IMPORTANT--refresh the for Coventry, UK
    }


    //Method for successful service
    @Override
    public void serviceSuccess(Channel channel) {
        dialog.hide(); //hide the dialog

        Item item = channel.getItem(); //Get item from channel
        //Get the resources by getting the identifier that gets the weather that depends on the code
        //that indicates the weather condition
        int resourceId = getResources().getIdentifier("drawable/icon_" + item.getCondition().getCode(), null, getPackageName());


        @SuppressWarnings("deprecation")
        //Create a new Drawable that stores the resource location for the weather icon
        Drawable weatherIconDrawable = getResources().getDrawable(resourceId);

        //Setting the weather icon from the drawable weather icon drawable
        weatherIconImageView.setImageDrawable(weatherIconDrawable);
        //Setting the text for the temperature that gets the temperature from the condition,
        //the degree sign in unicode, and the units.
        temperatureTextView.setText(item.getCondition().getTemperature() + "\u00B0" + channel.getUnits().getTemperature());
        //Setting the text for the condition by getting the description from the condition
        conditionTextView.setText(item.getCondition().getDescription());
        //Setting the text for the location by getting the location from the service
        locationTextView.setText(service.getLocation());
    }

    //Method for when the service fails
    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide(); //Hide the dialog
        //Show the error message by making a toast
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}

package com.example.oliver.weatherapp.data;

import org.json.JSONObject;

/**
 * Created by Oliver on 15/06/2015.
 */

//Weather channel that implements the JSON Populator interface
public class Channel implements JSONPopulator {
    //The channel that is going to use the class Item and Units
    private Item item; //Declare an item
    private Units units; //Declare the units

    public Item getItem() {
        return item;
    } //Getter to get the item

    public Units getUnits() {
        return units;
    } //Getter to get the units

    //Method that populates using the JSON Object data
    @Override
    public void populate(JSONObject data) {
        units = new Units(); //Declare new units
        units.populate(data.optJSONObject("units")); //Populate the units with the JSON Object units

        item = new Item(); //Declare new item
        item.populate(data.optJSONObject("item")); //Populate the item with the JSON Object item
    }
}

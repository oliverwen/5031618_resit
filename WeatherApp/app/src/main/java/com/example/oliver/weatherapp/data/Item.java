package com.example.oliver.weatherapp.data;

import org.json.JSONObject;

/**
 * Created by Oliver on 15/06/2015.
 */

//Item class that implements the JSON Populator interface
public class Item implements JSONPopulator {

    //Call and get the class condition, then return the condition
    private Condition condition;
    public Condition getCondition() {
        return condition;
    }

    //Method that is used to populate using the JSON object data
    @Override
    public void populate(JSONObject data) {
        condition = new Condition(); //Declare a new temperature
        //Populate the condition with the JSON Object condition
        condition.populate(data.optJSONObject("condition"));
    }
}

package com.example.oliver.weatherapp.data;

import org.json.JSONObject;

/**
 * Created by Oliver on 15/06/2015.
 */

//Interface called JSON Populator that populates the data on a JSON Object
public interface JSONPopulator {
    void populate(JSONObject data);
}

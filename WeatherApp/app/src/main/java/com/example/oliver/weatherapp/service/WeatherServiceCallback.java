package com.example.oliver.weatherapp.service;

import com.example.oliver.weatherapp.data.Channel;

/**
 * Created by Oliver on 15/06/2015.
 */

//Callback interface for the weather service that either returning success or failure
public interface WeatherServiceCallback {
    void serviceSuccess(Channel channel); //void to carry out the channel
    void serviceFailure(Exception exception); //void to carry out the exception to show why the service failed
}

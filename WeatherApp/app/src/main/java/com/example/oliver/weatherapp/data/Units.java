package com.example.oliver.weatherapp.data;

import org.json.JSONObject;

/**
 * Created by Oliver on 15/06/2015.
 */

//Class that implements the interface JSONPopulator
public class Units implements JSONPopulator{
    //Declare a string that is stores the temperature
    private String temperature;
    //Getter that gets and returns the temperature
    public String getTemperature() {
        return temperature;
    }

    //Method that is used to populate using the JSON Object data
    @Override
    public void populate(JSONObject data) {
        temperature = data.optString("temperature"); //Store the temperature with the JSON object temperature
    }
}

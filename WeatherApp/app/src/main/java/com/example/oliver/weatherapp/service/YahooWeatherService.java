package com.example.oliver.weatherapp.service;

import android.net.Uri;
import android.os.AsyncTask;

import com.example.oliver.weatherapp.data.Channel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Oliver on 15/06/2015.
 */

//Class that provides the weather service and refreshes the data displayed
public class YahooWeatherService {
    private WeatherServiceCallback callback; //declaring a Weather Service Callback
    private String location; //declaring a string that will contain the location
    private Exception error; //declaring a exception that will contain the error

    //Constructor of the class
    public YahooWeatherService(WeatherServiceCallback callback) {
        this.callback = callback;
    }

    //Getter that is returning the location
    public String getLocation() {
        return location;
    }

    //Void that is going the refresh the weather of the location l
    public void refreshWeather(String l) {
        this.location = l; //get the location l

        //New task that synchronises all the data
        new AsyncTask<String, Void, String>() {

            //Method that is going to run in the background
            @Override
            protected String doInBackground(String... strings) {

                //String containing Yahoo Query Language that select everything from the weather forecast in geo.places
                // where the text equals the location given and the units for the temperature is Celsius
                String YQL = String.format("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%s\") and u='c'", strings[0]);

                //String containing the url of the endpoint and encode the YQL
                String endpoint = String.format("https://query.yahooapis.com/v1/public/yql?q=%s&format=json", Uri.encode(YQL));

                //Try to execute the code and see if returns an error
                try {
                    URL url = new URL(endpoint); //Declaring a new url containing the endpoint

                    URLConnection connection = url.openConnection(); //Open the connection to the endpoint

                    InputStream inputStream = connection.getInputStream(); //Stream the connection

                    //Declaring a new buffer reader that contains a new input stream reader that reads the input stream
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder result = new StringBuilder(); //Build a new string containing the result
                    String line; //Declaring a new string called line

                    //Read every line and append it to the show the result
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    return result.toString(); //return the result as a string

                } catch (Exception e) {
                    error = e; //the error
                }
                return null;
            }

            //Method that is paired with the doInBackground method and returns a result
            @Override
            protected void onPostExecute(String s) {

                //if the string contains nothing and there is an error
                if (s == null && error != null) {
                    callback.serviceFailure(error); //then call the service failure function
                    return;
                }

                //Try to execute everything inside and see if it returns a error
                try {
                    //Declare a new JSON object that stores the data
                    JSONObject data = new JSONObject(s);

                    //Declare a JSON object that stores the query result of the data
                    JSONObject queryResult = data.optJSONObject("query");

                    //Declare an integer that is stores the count of the query result
                    int count = queryResult.optInt("count");

                    //If the count is zero, then call the serviceFailure function and display
                    //a message that tells the user that the location isn't found
                    if (count == 0) {
                        callback.serviceFailure(new LocationWeatherException("No weather information found for " + location));
                        return;
                    }

                    //Declare a new channel
                    Channel channel = new Channel();
                    //Populate the channel with the query result and the channel
                    channel.populate(queryResult.optJSONObject("results").optJSONObject("channel"));

                    //Call the function serviceSuccess
                    callback.serviceSuccess(channel);

                } catch (JSONException e) {
                   callback.serviceFailure(e); //Call the serviceFailure if there is an error
                }
            }
        }.execute(location); //execute using the specified location
    }

    //Method that is used to display a detailed error message
    public class LocationWeatherException extends Exception {
        public LocationWeatherException(String detailMessage) {
            super(detailMessage);
        }
    }
}

package com.example.oliver.contactsapp;

import android.net.Uri;


/**
 * Created by Oliver on 09/06/2015.
 */
public class Contact {
    //private variables declared for id, contact name, phone number, email, address and contact image
    private String _name, _phone, _email, _address;
    private Uri _imageURI;
    private int _id;

    //Public method declared that contains the private variables with values that will be used by other java classes
    public Contact (int id, String name, String phone, String email, String address, Uri imageURI) {
        _id = id;
        _name = name;
        _phone = phone;
        _email = email;
        _address = address;
        _imageURI = imageURI;
    }

    //getters created to get the values used by other java classes

    public int get_id(){
        return _id;
    }

    public String get_name() {
        return _name;
    }

    public String get_phone() {
        return _phone;
    }

    public String get_address() {
        return _address;
    }

    public String get_email() {
        return _email;
    }

    public Uri get_imageURI() {
        return _imageURI;
    }

    //setters created to set the values used by other java classes

    public void set_name(String _name) {
        this._name = _name;
    }

    public void set_phone(String _phone) {
        this._phone = _phone;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public void set_address(String _address) {
        this._address = _address;
    }

    public void set_imageURI(Uri _imageURI) {
        this._imageURI = _imageURI;
    }

}

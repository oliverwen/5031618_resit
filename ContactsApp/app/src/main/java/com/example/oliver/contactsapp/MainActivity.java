package com.example.oliver.contactsapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    //private static integer declared where EDIT is 0 and DELETE is 1
    //for the menu when long clicking a contact
    private static final int EDIT = 0, DELETE = 1;

    //public static variables that can be used by other java classes
    public static Context mainContext; //Context variable declared with the name main context
    public static List<Contact> Contacts = new ArrayList<>(); //New contact array list declared with the name Contacts
    public static int longClickedItemIndex; //Integer declared that will contain the index of the long clicked contact
    public static DatabaseHandler dbHandler; //DatabaseHandler that will be used to manage the contacts
    public static ArrayAdapter<Contact> contactAdapter; //ArrayAdapter that will used to handle the contact list array
    public static ListView contactListView; //ListView variable set for the contact list


    //EditText variables set for Contact Name, Phone, Email and Address
    //ImageView variable set for contact image
    //Uri variable set to contain the default contact image

    EditText nameTxt, phoneTxt, emailTxt, addressTxt;
    ImageView contactImageImgView;
    Uri imageUri = Uri.parse("android.resource://com.example.oliver.contactsapp/drawable/no_user_logo.png");





    //When the app starts, create everything inside this method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Showing the content from the layout file activity_main
        setContentView(R.layout.activity_main);

        //The variable nameTxt will contain the view of the EditText widget with the id txtName
        //i.e. the contact name from the layout file activity_main

        //The variable phoneTxt will contain the view of the EditText widget with the id txtPhone
        //i.e. the phone number from the layout file activity_main

        //The variable emailTxt will contain the view of the EditText widget wth the id txtEmail
        //i.e. the email address from the layout file activity_main

        //The variable addressTxt will contain the view of the EditText widget with the id txtAddress
        //i.e. the postal address from the layout file activity_main

        //The variable contactListView will contain the view of the EditText widget with the id listView
        //i.e. the contact list from the layout file activity_main

        //The variable contactImageImgView will contain the view of the ImageView widget with the id
        //imgViewContactImg i.e. the contact image from the layout file activity_main

        //The DatabaseHandler dbHandler will contain a new DatabaseHandler that will get the context
        //of the app
        nameTxt = (EditText) findViewById(R.id.txtName);
        phoneTxt = (EditText) findViewById(R.id.txtPhone);
        emailTxt = (EditText) findViewById(R.id.txtEmail);
        addressTxt = (EditText) findViewById(R.id.txtAddress);
        contactListView = (ListView) findViewById(R.id.listView);
        contactImageImgView = (ImageView) findViewById(R.id.imgViewContactImg);
        dbHandler = new DatabaseHandler(getApplicationContext());

        registerForContextMenu(contactListView);
        contactListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                longClickedItemIndex = position;
                return false;
            }
        });

        //The TabHost variable tabHost will contain the view of the TabHost widget with the id tabHost
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup(); //setting up the tabHost
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("creator"); //New tab with tabSpec "creator"
        tabSpec.setContent(R.id.tabCreator); //Tab content set to the id tabCreator, i.e. the creator tab
        tabSpec.setIndicator("Creator"); //Set the indicator for the tab which is the tab name
        tabHost.addTab(tabSpec); //Add the tab in the tabHost


        tabSpec = tabHost.newTabSpec("list"); //Create a new tabSpec "list"
        tabSpec.setContent(R.id.tabContactList); //Set to contain the content of id tabContactList
        tabSpec.setIndicator("List"); //Set the indicator to have the name "List"
        tabHost.addTab(tabSpec); //Add the tab to the tabHost

        final Button addBtn = (Button) findViewById(R.id.btnAdd);
        //Sets method for when the add button is clicked
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create a new contact that contains the count in the database, the string value of all the contact information and the imageUri.
                Contact contact = new Contact(dbHandler.getContactsCount(), String.valueOf(nameTxt.getText()), String.valueOf(phoneTxt.getText()), String.valueOf(emailTxt.getText()), String.valueOf(addressTxt.getText()), imageUri);
                //If the contact doesn't exist
                if (!contactExists(contact)) {
                    dbHandler.createContact(contact); //Ask database handler to create new contact
                    Contacts.add(contact); //Add contact to the Contacts list
                    contactAdapter.notifyDataSetChanged(); //Notify the contact adapter that the data has changed
                    //Shows a message to show that the new contact has been added to contact list.
                    Toast.makeText(getApplicationContext(), String.valueOf(nameTxt.getText()) + " has been added to your contacts!", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Shows a message to show that the contact already exists
                Toast.makeText(getApplicationContext(), String.valueOf(nameTxt.getText()) + " already exists! Please use a different name.", Toast.LENGTH_SHORT).show();
            }
        });

        //Method for the contact name for the text inside the EditText widget
        nameTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Nothing
            }

            @TargetApi(Build.VERSION_CODES.GINGERBREAD)
            @Override
            //When the text of the contact name changes
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Enable the add button if the number of characters of the contact name is greater than 0
                addBtn.setEnabled(String.valueOf(nameTxt.getText()).trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Nothing
            }

        });

        //Setting a new OnClickListener for the image view so that the image can be clicked to do something
        contactImageImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declaring a new Intent called newImage and set the type as image
                //Setting the action of newImage to get the content of the image
                //The type is set as "image/*" so that the user can get any image as long as it's
                //stored inside the sd card or internal storage
                //Starting the activity to show the result of choosing an image
                //The activity is called "Select Contact Image"

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select contact image"), 1);

                //If no image has been chosen, then set the image as the default image URI
                if (contactImageImgView == null) {
                    contactImageImgView.setImageURI(imageUri);
                }
            }
        });

        //If the number of contacts does not equal to zero, then get all the contacts and add them
        //to the contact list
        if (dbHandler.getContactsCount() != 0) {
            Contacts.addAll(dbHandler.getAllContacts());
        }

            //Populate the list
            populateList();

    }

    //Function to create a menu that will be used in the next function for when the item is selected
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        menu.setHeaderIcon(R.drawable.pencil_icon); //Set the header icon
        menu.setHeaderTitle("Contact Options"); //Set the title
        menu.add(Menu.NONE, EDIT, menu.NONE, "Edit Contact"); //add the option item "Edit Contact"
        menu.add(Menu.NONE, DELETE, menu.NONE, "Delete Contact"); //add the option item "Delete Contact"
    }

    //Function to use when one of the items from the menu is selected
    public boolean onContextItemSelected(MenuItem item) {

        //Switch statement that gets the id of the option items
        switch (item.getItemId()){

            //If the id is EDIT, create a new Intent that gets the application context of the
            //EditContact class and start the activity to show the result of the Intent
            case EDIT:
                Intent editContactIntent = new Intent(getApplicationContext(), EditContact.class);
                startActivityForResult(editContactIntent, 2);
                contactAdapter.notifyDataSetChanged();
                break;

            //If the id is DELETE, use the database handler to delete the longClicked contact from
            //the database and remove the longClicked contact from Contacts
            //Notify the contact adapter that the data has changed
            case DELETE:
                dbHandler.deleteContact(Contacts.get(longClickedItemIndex));
                Contacts.remove(longClickedItemIndex);
                contactAdapter.notifyDataSetChanged();
                break;
        }

        //Return the result of the selection
        return super.onContextItemSelected(item);
    }

    //Function for when the contact exists
    private boolean contactExists(Contact contact) {
        //Declare a string called name with the value of the name in variable contact
        String name = contact.get_name();

        //Declare an integer called contactCount to equal the size of the contact list
        int contactCount = Contacts.size();

        //For each contact in Contacts
        for (int i = 0;  i < contactCount; i++) {
            //Compare each of the names to check if they already exist and ignoring upper and lower cases
            if (name.compareToIgnoreCase(Contacts.get(i).get_name()) == 0) {
                return true; //if the contact exists then return true
            }
        }
        return false;
    }

    //Method capture the start activity for result
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        //Check if intent is ok by checking the if the result code is RESULT_OK
        if (resCode == RESULT_OK) {
            //If the request code is 1
            if(reqCode == 1) {
                //Update the image URI
                imageUri = data.getData();
                contactImageImgView.setImageURI(imageUri);
            //otherwise if the request code is 2
            } else if (reqCode == 2) {
                //notify the contact adapter that the data has changed
                //and populate the list
                contactAdapter.notifyDataSetChanged();
                populateList();
            }
        }
    }

    //new void created to populate contact list
    public void populateList() {
        //contact adapter is set to a new contact list adapter
        //and contact list view is set to contain the contact adapter.
        contactAdapter = new ContactListAdapter();
        contactListView.setAdapter(contactAdapter);
    }


    //New private class declared as a contact list adapter that extends the contact array adapter
    public class ContactListAdapter extends ArrayAdapter<Contact> {
        //The constructor is going to use the layout from listview_item for the Contacts list
        public ContactListAdapter() {
            super(MainActivity.this, R.layout.listview_item, Contacts);
        }


        @Override
        //public View method declared containing the position, view and view group
        public View getView(int position, View view, ViewGroup parent) {

            //If the view contains nothing, inflate the view to contain the layout of listview_item
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.listview_item, parent, false);
            }

            //Contact variable declared that is going to contain the position of the current contact
            Contact currentContact = Contacts.get(position);

            //TextView variable name declared that is going to contain the view of the contact name
            //Set the text in name to contain the name of the current contact
            TextView name = (TextView) view.findViewById(R.id.contactName);
            name.setText(currentContact.get_name());

            //TextView variable phone declared that is going to contain the view of the phone number
            //Set the text in phone to contain the phone number of the current contact
            TextView phone = (TextView) view.findViewById(R.id.phoneNumber);
            phone.setText(currentContact.get_phone());

            //TextView variable email declared that is going to contain the view of the email address
            //Set the text in email to contain the email address of the current contact
            TextView email = (TextView) view.findViewById(R.id.emailAddress);
            email.setText(currentContact.get_email());

            //TextView variable address declared that is going to contain the view of the contact address
            //Set the text in address to contain the postal address of the current contact
            TextView address = (TextView) view.findViewById(R.id.cAddress);
            address.setText(currentContact.get_address());

            //ImageView variable ivContactImage declared that is going to contain the view of the contact image
            //Set the URI in ivContactImage to contain the URI of the current contact
            ImageView ivContactImage = (ImageView) view.findViewById(R.id.ivContactImage);
            ivContactImage.setImageURI(currentContact.get_imageURI());

            //Return to show the view
            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

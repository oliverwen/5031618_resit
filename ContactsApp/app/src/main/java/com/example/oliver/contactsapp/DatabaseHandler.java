package com.example.oliver.contactsapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oliver on 10/06/2015.
 */

//Declaring a database handler that extends on the SQLiteOpenHelper
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1; //Set the database version

    //Setting up the fields for the database
    private static final String DATABASE_NAME = "contactManager",
            TABLE_CONTACTS = "contacts",
            KEY_ID = "id",
            KEY_NAME = "name",
            KEY_PHONE = "phone",
            KEY_EMAIL = "email",
            KEY_ADDRESS = "address",
            KEY_IMAGEURI = "imageUri";

    //Constructor for the database handler which needs the context, the database name
    //and the database version
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    //onCreate method that executes the SQL commands to create a table containing all the fields
    //inside the database
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_CONTACTS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME
                + " TEXT," + KEY_PHONE + " TEXT," + KEY_EMAIL + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_IMAGEURI + " TEXT)");
    }

    //onUpgrade method that executes the SQL that tells the database to delete the contacts table if the table exists
    //and then call the onCreate function to create a new table
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        onCreate(db);
    }

    //method for when creating a new contact
    public void createContact(Contact contact) {
        SQLiteDatabase db = getWritableDatabase(); //Allow writable access to the database
        ContentValues values = new ContentValues(); //Create new content values
        values.put(KEY_NAME, contact.get_name()); //Put the contact name in KEY_NAME
        values.put(KEY_PHONE, contact.get_phone()); //Put the phone number in KEY_PHONE
        values.put(KEY_EMAIL, contact.get_email()); //Put the email address in KEY_EMAIL
        values.put(KEY_ADDRESS, contact.get_address()); //Put the postal address in KEY_ADDRESS

        //Put the string value of the image URI in KEY_IMAGEURI
        values.put(KEY_IMAGEURI, contact.get_imageURI().toString());


        db.insert(TABLE_CONTACTS, null, values); //Insert the values of the TABLE_CONTACTS to the database
        db.close(); //close the database
    }

    //Method used to get a contact
    public Contact getContact(int id) {
        SQLiteDatabase db = getReadableDatabase(); //Allow readable access to the database

        //A cursor created that selects each database field by passing a query with the table,
        //a string with all the fields, the key id of the current contact and array which contains
        //the string values of the cursor id
        Cursor cursor = db.query(TABLE_CONTACTS, new String[]{
                KEY_ID, KEY_NAME, KEY_PHONE, KEY_EMAIL, KEY_ADDRESS, KEY_IMAGEURI
        }, KEY_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);

        //If the cursor does not equal to null, then move the cursor to the first contact
        if (cursor != null) {
            cursor.moveToFirst();
        }

        //Create a new contact and pass through the column numbers of all the fields
        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), Uri.parse(cursor.getString(5)));
        db.close(); //close the database
        cursor.close(); //close the cursor
        return  contact; //return the contact
    }

    //Method for deleting contact
    public void deleteContact(Contact contact) {
        SQLiteDatabase db = getWritableDatabase(); //Allow writable access to the database
        //Inside the database, delete the row specified by the KEY_ID in the table
        db.delete(TABLE_CONTACTS, KEY_ID + "= ?", new String[]{String.valueOf(contact.get_id())});
        db.close(); //close the database
    }

    //Count everything from TABLE_CONTACTS then close the database and the cursor
    public int getContactsCount() {
        SQLiteDatabase db = getReadableDatabase(); //allow readable access
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_CONTACTS, null); //select everything from the table
        int count = cursor.getCount();
        db.close();
        cursor.close();

        return count; //return the count
    }

    //Method to update the contact
    public int updateContact(Contact contact) {
        SQLiteDatabase db = getWritableDatabase(); //Allow writable access to the database
        ContentValues values = new ContentValues(); //Declare new content values

        values.put(KEY_NAME, contact.get_name());  //put the contact name to KEY_NAME
        values.put(KEY_PHONE, contact.get_phone()); //put the contact name to KEY_PHONE
        values.put(KEY_EMAIL, contact.get_email()); //put the contact name to KEY_EMAIL
        values.put(KEY_ADDRESS, contact.get_address()); //put the contact name to KEY_ADDRESS
        values.put(KEY_IMAGEURI, contact.get_imageURI().toString()); //put the string value of the image URI to KEY_IMAGEURI

        //Declaring an integer called rows affected that updates the database
        //with the table, the values and the id as parameters
        int rowsAffected = db.update(TABLE_CONTACTS, values, KEY_ID + " =?", new String[]{String.valueOf(contact.get_id())});


        db.close(); //close the database
        return rowsAffected; //return the integer

    }

    //Method to get all the contacts
    public List<Contact> getAllContacts() {
        //Create a new array list called Contact
        List<Contact> contacts = new ArrayList<Contact>();
        SQLiteDatabase db = getReadableDatabase(); //Get readable access to the database
        //Create a cursor that goes through everything in TABLE_CONTACTS
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_CONTACTS, null);

        //If the cursor moves to the first row
        if (cursor.moveToFirst()) {
            //Add a new contact with all the fields to the Contact list, while moving on to the next row
            do {
                contacts.add(new Contact(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), Uri.parse(cursor.getString(5))));
            }
            while (cursor.moveToNext());
        }
        cursor.close(); //Close the cursor
        db.close(); //Close the database

        return contacts; //return the contacts
    }
}

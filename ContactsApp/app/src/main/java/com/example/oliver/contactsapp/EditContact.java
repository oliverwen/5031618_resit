package com.example.oliver.contactsapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Oliver on 17/06/2015.
 */
public class EditContact extends Activity{
    //Declaring variables for the EditContact layout file for the database handler, image view,
    //text view and the image uri
    DatabaseHandler dbHandler = MainActivity.dbHandler;
    ImageView iv;
    TextView name, phone, email, address;
    Uri imageUri;

    //On create method created
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_contact); //setting the content view as the same as edit_contact

        //Declaring a variable for the long clicked item to be updated
        final Contact tempContact = MainActivity.Contacts.get(MainActivity.longClickedItemIndex);

        //Showing the current values of the contact and updating from each variable
        iv = (ImageView) findViewById(R.id.ivEditImage);
        iv.setImageURI(tempContact.get_imageURI());

        name = (TextView) findViewById(R.id.txtEditName);
        name.setText(tempContact.get_name());

        phone = (TextView) findViewById(R.id.txtEditPhone);
        phone.setText(tempContact.get_phone());

        email = (TextView) findViewById(R.id.txtEditEmail);
        email.setText(tempContact.get_email());

        address = (TextView) findViewById(R.id.txtEditAddress);
        address.setText(tempContact.get_address());

        //Setting the function for the edit button
        final Button btnDone = (Button) findViewById(R.id.btnEditDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //If the image uri is not empty set the image uri to the updated image uri
                if (imageUri != null) {
                    tempContact.set_imageURI(imageUri);
                }

                //Updating the contact name, phone number, email and address

                tempContact.set_name(String.valueOf(name.getText()));
                tempContact.set_phone(String.valueOf(phone.getText()));
                tempContact.set_email(String.valueOf(email.getText()));
                tempContact.set_address(String.valueOf(address.getText()));

               //Updating the Contact array via the database handler
                dbHandler.updateContact(tempContact);

                //Notifying the contactAdapter that the data has changed
                MainActivity.contactAdapter.notifyDataSetChanged();


                //Declaring a new Intent called return Intent and setting the result to return OK
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);

                //Show a message the contact has been updated
                Toast.makeText(getApplicationContext(), String.valueOf(name.getText()) + " has been updated in your contacts!", Toast.LENGTH_SHORT).show();

                //Finish and close the window to show the contents in the contact list tab
                finish();

            }
        });

        //Setting a new OnClickListener for the image view so that the image can be clicked to do something
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Declaring a new Intent called newImage and set the type as image
                //Setting the action of newImage to get the content of the image
                //The type is set as "image/*" so that the user can get any image as long as it's
                //stored inside the sd card or internal storage
                //Starting the activity to show the result of choosing an image
                //The activity is called "Select Contact Image"

                Intent newImage = new Intent();
                newImage.setType("image/*");
                newImage.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(newImage, "Select Contact Image"), 1);
            }
        });

    }

    //Function to capture the start activity for result
    public void onActivityResult(int reqCode, int resCode, Intent data){
        //Check if intent is ok by checking the if the result code is RESULT_OK
        if(resCode == RESULT_OK) {
            //If the request code is 1
            if(reqCode == 1) {
                //Update the image URI
                imageUri = data.getData();
                iv.setImageURI(imageUri);
            }
        }
    }


}
